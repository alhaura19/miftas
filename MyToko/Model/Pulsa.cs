﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyToko.Model
{
    public class Pulsa
    {
        private string jenisKartu;
        public string JenisKartu
        {
            get { return jenisKartu; }
            set { jenisKartu = value; }
        }

        private string topUp;
        public string TopUp
        {
            get { return topUp; }
            set { topUp = value; }
        }

        private string nomorHP;
        public string NomorHP
        {
            get { return nomorHP; }
            set { nomorHP = value; }
        }
    }
}
