﻿namespace MyToko.View
{
    partial class MyToko
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.metroTabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.tabPulsa = new System.Windows.Forms.TabPage();
            this.buttonUpdate = new MetroFramework.Controls.MetroButton();
            this.textboxNomor = new MetroFramework.Controls.MetroTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboxNominal = new MetroFramework.Controls.MetroComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboxKartu = new MetroFramework.Controls.MetroComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gridTable = new MetroFramework.Controls.MetroGrid();
            this.tabAksesoris = new System.Windows.Forms.TabPage();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusDate = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.metroTabControl1.SuspendLayout();
            this.tabPulsa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTable)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroTabControl1
            // 
            this.metroTabControl1.Controls.Add(this.tabPulsa);
            this.metroTabControl1.Controls.Add(this.tabAksesoris);
            this.metroTabControl1.Location = new System.Drawing.Point(7, 63);
            this.metroTabControl1.Name = "metroTabControl1";
            this.metroTabControl1.SelectedIndex = 0;
            this.metroTabControl1.Size = new System.Drawing.Size(696, 309);
            this.metroTabControl1.TabIndex = 0;
            this.metroTabControl1.UseSelectable = true;
            // 
            // tabPulsa
            // 
            this.tabPulsa.Controls.Add(this.buttonUpdate);
            this.tabPulsa.Controls.Add(this.textboxNomor);
            this.tabPulsa.Controls.Add(this.label3);
            this.tabPulsa.Controls.Add(this.comboxNominal);
            this.tabPulsa.Controls.Add(this.label2);
            this.tabPulsa.Controls.Add(this.comboxKartu);
            this.tabPulsa.Controls.Add(this.label1);
            this.tabPulsa.Controls.Add(this.gridTable);
            this.tabPulsa.Location = new System.Drawing.Point(4, 38);
            this.tabPulsa.Name = "tabPulsa";
            this.tabPulsa.Size = new System.Drawing.Size(688, 267);
            this.tabPulsa.TabIndex = 0;
            this.tabPulsa.Text = "Pulsa";
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Location = new System.Drawing.Point(130, 140);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(75, 23);
            this.buttonUpdate.TabIndex = 8;
            this.buttonUpdate.Text = "Update";
            this.buttonUpdate.UseSelectable = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // textboxNomor
            // 
            // 
            // 
            // 
            this.textboxNomor.CustomButton.Image = null;
            this.textboxNomor.CustomButton.Location = new System.Drawing.Point(115, 1);
            this.textboxNomor.CustomButton.Name = "";
            this.textboxNomor.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.textboxNomor.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.textboxNomor.CustomButton.TabIndex = 1;
            this.textboxNomor.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.textboxNomor.CustomButton.UseSelectable = true;
            this.textboxNomor.CustomButton.Visible = false;
            this.textboxNomor.Lines = new string[0];
            this.textboxNomor.Location = new System.Drawing.Point(70, 95);
            this.textboxNomor.MaxLength = 32767;
            this.textboxNomor.Name = "textboxNomor";
            this.textboxNomor.PasswordChar = '\0';
            this.textboxNomor.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.textboxNomor.SelectedText = "";
            this.textboxNomor.SelectionLength = 0;
            this.textboxNomor.SelectionStart = 0;
            this.textboxNomor.ShortcutsEnabled = true;
            this.textboxNomor.Size = new System.Drawing.Size(137, 23);
            this.textboxNomor.TabIndex = 7;
            this.textboxNomor.UseSelectable = true;
            this.textboxNomor.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.textboxNomor.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("HelveticaNeue", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(77)));
            this.label3.Location = new System.Drawing.Point(3, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Nomor:";
            // 
            // comboxNominal
            // 
            this.comboxNominal.FormattingEnabled = true;
            this.comboxNominal.ItemHeight = 23;
            this.comboxNominal.Location = new System.Drawing.Point(70, 55);
            this.comboxNominal.Name = "comboxNominal";
            this.comboxNominal.Size = new System.Drawing.Size(137, 29);
            this.comboxNominal.TabIndex = 5;
            this.comboxNominal.UseSelectable = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("HelveticaNeue", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(77)));
            this.label2.Location = new System.Drawing.Point(3, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Nominal:";
            // 
            // comboxKartu
            // 
            this.comboxKartu.FormattingEnabled = true;
            this.comboxKartu.ItemHeight = 23;
            this.comboxKartu.Location = new System.Drawing.Point(70, 15);
            this.comboxKartu.Name = "comboxKartu";
            this.comboxKartu.Size = new System.Drawing.Size(137, 29);
            this.comboxKartu.TabIndex = 3;
            this.comboxKartu.UseSelectable = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("HelveticaNeue", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(77)));
            this.label1.Location = new System.Drawing.Point(3, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Kartu:";
            // 
            // gridTable
            // 
            this.gridTable.AllowUserToResizeRows = false;
            this.gridTable.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridTable.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridTable.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.gridTable.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridTable.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.gridTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridTable.DefaultCellStyle = dataGridViewCellStyle5;
            this.gridTable.EnableHeadersVisualStyles = false;
            this.gridTable.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.gridTable.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridTable.Location = new System.Drawing.Point(300, 3);
            this.gridTable.Name = "gridTable";
            this.gridTable.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridTable.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.gridTable.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridTable.Size = new System.Drawing.Size(385, 261);
            this.gridTable.TabIndex = 0;
            // 
            // tabAksesoris
            // 
            this.tabAksesoris.Location = new System.Drawing.Point(4, 38);
            this.tabAksesoris.Name = "tabAksesoris";
            this.tabAksesoris.Size = new System.Drawing.Size(688, 267);
            this.tabAksesoris.TabIndex = 1;
            this.tabAksesoris.Text = "Aksesoris";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusDate,
            this.toolStripProgressBar1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 372);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(706, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // statusDate
            // 
            this.statusDate.Name = "statusDate";
            this.statusDate.Size = new System.Drawing.Size(151, 17);
            this.statusDate.Text = "                                                ";
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(536, 16);
            // 
            // MyToko
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(710, 395);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.metroTabControl1);
            this.Name = "MyToko";
            this.Text = "MyToko";
            this.Load += new System.EventHandler(this.MyToko_Load);
            this.metroTabControl1.ResumeLayout(false);
            this.tabPulsa.ResumeLayout(false);
            this.tabPulsa.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTable)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl metroTabControl1;
        private System.Windows.Forms.TabPage tabPulsa;
        private System.Windows.Forms.TabPage tabAksesoris;
        private MetroFramework.Controls.MetroComboBox comboxKartu;
        private System.Windows.Forms.Label label1;
        private MetroFramework.Controls.MetroGrid gridTable;
        private MetroFramework.Controls.MetroTextBox textboxNomor;
        private System.Windows.Forms.Label label3;
        private MetroFramework.Controls.MetroComboBox comboxNominal;
        private System.Windows.Forms.Label label2;
        private MetroFramework.Controls.MetroButton buttonUpdate;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel statusDate;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
    }
}

