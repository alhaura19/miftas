﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using MetroFramework.Controls;
using MySql.Data.MySqlClient;
using MyToko.Control;

namespace MyToko.View
{
    public partial class MyToko : MetroForm
    {
        private PulsaController PulsaController;

        public MyToko()
        {
            InitializeComponent();
            PulsaController = new PulsaController();
        }

        public MetroGrid TablePulsa
        {
            get { return gridTable; }
            set { gridTable = value; }
        }

        private void MyToko_Load(object sender, EventArgs e)
        {
            PulsaController.Show(TablePulsa);
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {

        }
    }
}
