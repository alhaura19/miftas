﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MetroFramework.Controls;
using MySql.Data.MySqlClient;
using MyToko.Model;
using System.Data;

namespace MyToko.Control
{
    public class PulsaController
    {
        private Pulsa Pulsa;
        private MySqlDataAdapter mySqlDataAdapter;

        public PulsaController()
        {
            Pulsa = new Pulsa();
        }

        public void Show(MetroGrid tablePulsa)
        {
            var dbCon = DBConnection.Instance();
            dbCon.DatabaseName = "toko";

            if (dbCon.IsConnect())
            {
                mySqlDataAdapter = new MySqlDataAdapter("select * from kartu", dbCon.Connection);
                DataSet DS = new DataSet();
                mySqlDataAdapter.Fill(DS);
                tablePulsa.DataSource = DS.Tables[0];

                dbCon.Close();
            }
        }

        public void Update()
        {

        }

        public void Insert()
        {

        }

    }
}
